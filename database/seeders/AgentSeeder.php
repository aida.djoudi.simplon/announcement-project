<?php

namespace Database\Seeders;

use App\Models\Agent;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AgentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //add 3 instances of agent
        $agent = new Agent();
        $agent->firstName = "luca";
        $agent->lastName = "Levis";
        $agent->save();

        $agent = new Agent();
        $agent->firstName = "Stephane";
        $agent->lastName = "SOULET";
        $agent->save();

        $agent = new Agent();
        $agent->firstName = "Cedric";
        $agent->lastName = "WAUCAMPT";
        $agent->save();

        $agent = new Agent();
        $agent->firstName = "Rafia";
        $agent->lastName = "Amar";
        $agent->save();

        $agent = new Agent();
        $agent->firstName = "Meyssa";
        $agent->lastName = "Djoudi";
        $agent->save();

        $agent = new Agent();
        $agent->firstName = "Meyar";
        $agent->lastName = "Djoudi";
        $agent->save();
    }
}
