<?php

namespace Database\Seeders;

use App\Models\Announcement;
use Illuminate\Database\Seeder;

class AnnouncementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //add 3 instances of announcement 
        $announcement  = new Announcement();
        $announcement ->reference = "1234AZ";
        $announcement ->picture = "picture1.jpg";
        $announcement ->price = 91000.00;
        $announcement ->area = 71;
        $announcement ->roomsNumber = 2;
        $announcement ->agent_id = 1;
        $announcement ->save();

        $announcement  = new Announcement();
        $announcement ->reference = "1234BZ";
        $announcement ->picture = "picture2.jpg";
        $announcement ->price = 80000.50;
        $announcement ->area = 50;
        $announcement ->roomsNumber = 4;
        $announcement ->agent_id = 2;
        $announcement ->save();

        $announcement  = new Announcement();
        $announcement ->reference = "1234CZ";
        $announcement ->picture = "picture3.jpg";
        $announcement ->price = 86000.70;
        $announcement ->area = 60;
        $announcement ->roomsNumber = 5;
        $announcement ->agent_id = 3;
        $announcement ->save();

        $announcement  = new Announcement();
        $announcement ->reference = "1234DZ";
        $announcement ->picture = "picture4.jpg";
        $announcement ->price = 90000.00;
        $announcement ->area = 70;
        $announcement ->roomsNumber = 2;
        $announcement ->agent_id = 1;
        $announcement ->save();

        $announcement  = new Announcement();
        $announcement ->reference = "1234EZ";
        $announcement ->picture = "picture5.jpg";
        $announcement ->price = 91000.50;
        $announcement ->area = 70;
        $announcement ->roomsNumber = 4;
        $announcement ->agent_id = 2;
        $announcement ->save();

        $announcement  = new Announcement();
        $announcement ->reference = "1234FZ";
        $announcement ->picture = "picture6.jpg";
        $announcement ->price = 99000.70;
        $announcement ->area = 89;
        $announcement ->roomsNumber = 5;
        $announcement ->agent_id = 3;
        $announcement ->save();

    }
}
