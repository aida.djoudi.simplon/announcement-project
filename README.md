
## Estate Announcements 
This Estate announcements project is a Laravel 8.83.2 version web Application. 

⚡ Requirements

* A php and composer installed
* A simple database with MySQL2For more information
* Please check the official laravel installation guide for server requirements before you start.[official Documentation](https://laravel.com/docs/8.x/installation#getting-started-on-linux)

💻 Installation

1.Clone the repository
with ssh
```
git clone git@gitlab.com:aida.djoudi.simplon/announcement-project.git

```
or 

with https
```
git clone https://gitlab.com/aida.djoudi.simplon/announcement-project.git
```

2.Switch to the repo folder and open the project with your IDE 
```
cd announcement-project
```

3.Install all the dependencies using composer
```
composer install
```

4.create your databse 
```
mysql -u db_user -p
```
```
CREATE DATABASE db_name;
```
```
exit
```

5.Copy the example env file and make the required configuration changes in the .env file
```
cp .env.example .env
```

like this:

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=db_name(with the same name in your database created )
DB_USERNAME=db_user(the user name to acces in your database)
DB_PASSWORD=db_password(the password to acces in your database)

```

6.Generate a new application key
```
php artisan key:generate
```

7.Run the database migrations (Set the database connection in .env before migrating)
```
php artisan migrate
```

8.Run the database seeder 
```
php artisan db:seed --class=AgentSeeder
php artisan db:seed --class=AnnouncementSeeder
```


9.Start the local development server
```
php artisan serve
```

🔥 You can now access the server at http://localhost:8000

🏃 Command list

```
git clone https://gitlab.com/aida.djoudi.simplon/announcement-project.git
cd announcement-project
composer install
cp .env.example .env
php artisan key:generate
Make sure you set the correct database connection information before running the migrations Environment variables
php artisan migrate
php artisan db:seed --class=AgentSeeder
php artisan db:seed --class=AnnouncementSeeder
php artisan serve
```

🕺 Contribute

Want to hack on Estate announcements Project? Follow the next instructions:

Fork this repository to your own git account and then clone it to your local device
Install dependencies as we explained at this documentation.
Send a pull request 🙌


Good luck, developer! 🚀