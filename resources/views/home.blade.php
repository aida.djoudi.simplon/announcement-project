@extends('base')
@section("title", "Listes des annonces")
@section('content')

<!-- Start Intro Single-->
<section class="announcement_intro">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-lg-8">
        <div class="title-single-box">
          <h1 class="title-single">Nos incroyables annonces</h1>
          
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End Intro Single-->

<!-- We browse the collection of announcement   -->
<div class="album py-5 ">
    <div class="container">
      <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
        @foreach ($announcements as $announcement)    
        <div class="col">
          <div class="card shadow-sm">
            <img src="{{asset('img/'.$announcement->picture)}}"  alt="picture of the announcement" height=225 width=100%>
            <div class="card-body">
              <span class="price-a">{{round($announcement->price, 2)}} €</span>
              <p class="card-text">
                <div class="card-info d-flex justify-content-around">
                  <div>
                    <h4 class="card-info-title"><img id="logoMenu" src="{{asset('img/area.svg')}}"  alt="surface"></h4>
                    <span>{{$announcement->area}} m<sup>2</sup></span>
                  </div>
                  <div>
                    <h4 class="card-info-title"><img id="logoMenu" src="{{asset('img/room_number.svg')}}"  alt="number of room"></h4>
                    <span>{{$announcement->roomsNumber-1}} Chambre(s)</span>
                  </div>
                </div>
              </p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <a class="btn btn-sm btn-outline-secondary click" href="{{ route('announcements.show', $announcement ) }}" role="button">En savoir plus &raquo;</a>
                </div>
                <small class="text-muted">{{$announcement->reference}}</small>
              </div>
            </div>  
          </div>
        </div>
        @endforeach
      </div>
    </div>                        
</div>
@endsection


