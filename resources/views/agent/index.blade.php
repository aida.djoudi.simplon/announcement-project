@extends('base')
@section("title","index")
@section('content')
{{-- {{dump($agents)}} --}}
	<!-- The table to display the agents list -->
    </p>   
	<div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th>Nom </th>
                    <th>Prénom </th>
                    <th>Editer</th>
                    <th>Supprimer</th>
                </tr>
            </thead>
            <tbody>
			<!-- We browse the collection of agent  -->
			@foreach ($agents as $agent)
                <tr>
                    <td>{{ $agent->lastName }}</td>
                    <td>{{ $agent->firstName }}</td>
                    <td>
					<!-- Link to edit agent -->
                        <a href="{{ route('agents.edit', $agent) }}" 
                            title="edit the agent" ><i class="fas fa-edit fa-2x"></i>
                        </a>
                    </td>
                    <td>
					<!-- The form to delete agent-->
                        <form method="POST" action="{{ route('agents.destroy', $agent) }}" 
                            onsubmit="return confirm('Êtes-vous sûr de vouloir supprimer ?');"
                        >
                            <input type="hidden" name="_method" value="DELETE">
                            @csrf
                            <button class="btn btn-outline-danger"><i class="fas fa-trash-alt fa-2x"></i></button>
                        </form>																								
                    </td>
                </tr>
			@endforeach
            </tbody>
        </table>
    </div>
    <div>
        <a href="{{ route('agents.create') }}">
            <button class="btn btn-success offset-mr-5">
                <i class="fas fa-plus"></i> 
                    Ajouter un agent immobilier
            </button>
        </a>
    </div>	
@endsection



