@extends('base')
@section("title","edit")
@section('content')
    <!-- if we have agent -->
	@if (isset($agent))
    <!-- the form is managed by the route "agents.update" -->
	<form method="POST" action="{{ route('agents.update', $agent) }}">
        <input type="hidden" name="_method" value="PUT">
    @else 
    <!-- the form is managed by the route "agents.store" -->  
    <form method="POST" action="{{ route('agents.store') }}">
    @endif
    @csrf
    
    <p>
        <label for="lastName" >Nom</label><br/>
        <!-- if we have $agent->lastName, we complete the input value -->
        <input type="text" name="lastName" 
            value="{{ isset($agent->lastName) ? $agent->lastName : old('lastName') }}" 
            id="lastName" placeholder="Nom"
        >
        <!-- error message -->
        @error("lastName")
        <div>{{ $message }}</div>
        @enderror
    </p>
    <p>
        <label for="firstName" >Prénom</label><br/>
        <!-- if we have $agent->firstName, we complete the input value -->
        <input type="text" name="firstName" 
            value="{{ isset($agent->firstName) ? $agent->firstName : old('firstName') }}"  
            id="firstName" placeholder="Prénom"
        >
        <!-- error message -->
        @error("firstName")
        <div>{{ $message }}</div>
        @enderror
    </p>
        <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Valider </button>
    </form>
@endsection
