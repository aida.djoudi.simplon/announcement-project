<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>@yield("title")</title>
    <!-- Bootstrap core CSS -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" >
    <!--Custom style for this template-->
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <!--select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" /> 
    <!-- Favicons -->
    <link rel="icon" href="{{asset('img/logon.jpg')}}">
    <!-- range slider of price with nouislider -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/14.6.3/nouislider.min.css" rel="stylesheet" />
    <meta name="theme-color" content="#7952b3">
    <!--to add the style in template that extend base template-->
    @yield("styles")
</head>
<body>   
<header>
    @include('header')
</header>

<main>
    <div class="container">
        @include('flash-message')
        @yield('content')
    </div>
</main>

<footer class="page-footer font-small text-white bg-dark mt-4 pt-3">
    @include('footer')
</footer>
    <!-- JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{asset('js/boostrap.min.js')}}" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <!--to add the script in template that extend base template-->
    <script src="{{asset('js/select2agent.js')}}"></script>
    <!--to add the range slider with noUiSlider-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/14.6.3/nouislider.min.js"></script>
    @yield("scripts")
</body>
</html>
