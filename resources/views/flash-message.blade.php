<!-- success message flash -->
@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">  
        <strong>{{ $message }}</strong>
    </div>
@endif

<!-- error message flash -->    
@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
        <strong>{{ $message }}</strong>
    </div>
@endif

<!--  warning message flash -->     
@if ($message = Session::get('warning'))
    <div class="alert alert-warning alert-block">
        <strong>{{ $message }}</strong>
    </div>
@endif

<!-- info message flash -->     
@if ($message = Session::get('info'))   
    <div class="alert alert-info alert-block">
        <strong>{{ $message }}</strong>
    </div>   
@endif