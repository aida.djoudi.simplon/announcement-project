@extends('base')
@section("title","index")
@section('content')


{{-- {{dump($announcements)}}  --}}
	
    <div class= "row">
        <div class="col-3">
        <!-- The form of the filter by agent and price -->
            <form method="GET" action="{{ route('announcements.index') }}" enctype="multipart/form-data" >
                <h3>Liste des Agents</h3>
                <select class="form-control agents" id="agent" name="agent">
                    <option value="" selected=" ">Veuillez choisir...</option> 
                    @foreach ($agents as $agent)
                    {{-- <p><a href="/announcements?agent={{$agent->id}}">{{$agent->firstName}}</a></p> --}}
                    <option value="{{ $agent->id }}">{{ $agent->lastName.' '.$agent->firstName }}</option>
                    @endforeach  
                </select>
                <hr>
                <p>
                <h3>Price</h3>
                <div class="row ">
                    <input class="col inputMin" type="number" name="start_price" id="min" placeholder="Prix min"> 
                        <br>
                    <input class="col" type="number" name="end_price" id="max" placeholder="Prix max"> 
                </div>
                        <br>
                <div id="slider" style="margin-bottom: 3rem;"></div>
                <button type="submit" class="btn btn-success filter"> Filtrer </button>	
            </form>
        </div>
        <!-- The table to display the announcements list -->
        <div class="col-9 table-responsive">
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th>Agent </th>
                        <th>Reference</th>
                        <th>image </th>
                        <th>Prix </th>
                        <th>Surface habitable </th>
                        <th>Nombre de pièces </th>
                        <th>Voir </th>
                        <th>Editer</th>
                        <th>Supprimer</th>
                    </tr>
                </thead>
                <tbody>
			<!-- We browse the collection of announcement   -->
                    @foreach ($announcements as $announcement)
                    <tr>
                        <td>{{$announcement->agent->lastName.' '.$announcement->agent->firstName}}</td>
                        <td>{{$announcement->reference}}</td>
                        <td><img src="{{asset('img/'.$announcement->picture)}}"  
                            alt="picture of the announcement" height=50 width=50>
                        </td>
                        <td>{{$announcement->price}}€</td>
                        <td>{{$announcement->area}} m²</td>
                        <td>{{$announcement->roomsNumber}}</td>
                        <td>
            <!-- Link to show announcement  -->
                            <a href="{{ route('announcements.show', $announcement) }}" 
                                title="view the announcement" >
                                    <i class="fas fa-eye fa-2x"></i>
                            </a>
                        </td>
                        <td>
			<!-- Link to edit announcement  -->
                            <a href="{{ route('announcements.edit', $announcement ) }}" 
                                title="edit the announcement" >
                                <i class="fas fa-edit fa-2x"></i>
                            </a>
                        </td>
                        <td>
			<!-- The form to delete announcement -->
                            <form method="POST" action="{{ route('announcements.destroy', $announcement ) }}" 
                                onsubmit="return confirm('Êtes-vous sûr de vouloir supprimer ?');">
                                <input type="hidden" name="_method" value="DELETE">
                                @csrf
                                <button class="btn btn-outline-danger">
                                    <i class="fas fa-trash-alt fa-2x"></i>
                                </button>
                            </form>																								
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <a href="{{ route('announcements.create') }}">
                <button class="btn btn-success">
                    <i class="fas fa-plus"></i>
                        Ajouter une announce
                </button>
            </a>	   
        </div>
    <div>
@endsection
@section("scripts")
<script>
    var slider = document.getElementById('slider');
    if (slider) {
    const min = document.getElementById('min')
    const max = document.getElementById('max')
    const range = noUiSlider.create(slider, {
        start: [min.value || 100, max.value || 100000],
        connect: true,
        step:100,
        range: {
            'min': 100,
            'max': 100000
        }
    });
    range.on('slide', function (values, handle) {
        console.log(values, handle)
        if (handle === 0) {
            min.value = Math.round(values[0])
        }
        if (handle === 1) {
            max.value = Math.round(values[1])
        }
    })
}
</script>
@endsection



