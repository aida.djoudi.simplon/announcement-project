@extends('base')
@section("title","edit")

@section('content')
    <!-- if we have announcement -->
	@if (isset($announcement))
    <!-- the form is managed by the route "announcements.update" -->
        <form  method="POST" action="{{ route('announcements.update', $announcement) }}" 
            enctype="multipart/form-data">
            <input type="hidden" name="_method" value="PUT"
        >
    @else 
    <!-- the form is managed by the route "announcements.store" -->  
        <form method="POST" action="{{ route('announcements.store') }}" 
            enctype="multipart/form-data" 
        >
    @endif
    @csrf

    <!-- if we have  $announcement->picture, display it -->
    @if(isset($announcement->picture))
        <p>
            <img class="mx-auto d-block" src="{{ asset('img/'.$announcement->picture) }}" 
            alt="announcements picture" style="max-height: 400px;"
        >
        </p>
    @endif
        <p>
            <label for="picture" >Photo de l'annonce</label><br/>
            <input class="form-control" type="file" name="picture" id="picture"> 
        <!-- error message "picture" -->
            @error("picture")
                <div>{{ $message }}</div>
            @enderror
        </p>
    
        <p>
            <label for="reference" >Référence</label>
        <!-- if we have $announcement->reference, we complete the input value -->
            <input class="form-control" type="text" name="reference" 
                value="{{ isset($announcement->reference) ? $announcement->reference : old('reference') }}" 
                id="reference" placeholder="Référence"
            >
        <!-- error message -->
            @error("reference")
                <div>{{ $message }}</div>
            @enderror
        </p>

        <p>
            <label for="price">Prix</label>
        <!-- if we have $announcement->price, we complete the input value -->
            <input class="form-control" type="number" id="price" name="price" step="0.01" 
                value="{{ isset($announcement->price) ? $announcement->price : old('price') }}" 
                id="price" placeholder="Prix"
            >
        <!-- error message -->
            @error("reference")
                <div>{{ $message }}</div>
            @enderror
        </p>

        <p>
            <label for="area">Surface Habitable</label>
        <!-- if we have $announcement->area, we complete the input value -->
            <input class="form-control" type="number" id="area" name="area" step="0.01" 
                value="{{ isset($announcement->area) ? $announcement->area : old('area') }}" 
                id="area" placeholder="Surface Habitable"
            >
        <!-- error message -->
            @error("area")
                <div>{{ $message }}</div>
            @enderror
        </p>

        <p>
            <label for="roomsNumber">Nombre de pièces</label>
        <!-- if we have $announcement->nu, we complete the input value -->
            <input class="form-control" type="number" id="roomsNumber" name="roomsNumber"  
                value="{{ isset($announcement->roomsNumber) ? $announcement->roomsNumber : old('roomsNumber') }}" 
                id="roomsNumber" placeholder="Nombre de pièces"
            >
        <!-- error message -->
            @error("roomsNumber")
                <div>{{ $message }}</div>
            @enderror
        </p>
        <p>
            <label for="area"> Agent </label>
            <select class="form-control agents" id="agent_id" name="agent_id">
                <option value="" selected=" ">Veuillez choisir...</option>  
                @foreach($agents as $agent)
                    <option 
                    value="{{ isset($agent->id) ? $agent->id : old('id') }}"
                    @if($agent->id == $agent_id)
                    selected
                    @endif
                    >
                    {{ $agent->lastName.' '.$agent->firstName }}
                    </option>
                @endforeach
            </select>
            @error("agent_id")
                <div>{{ $message }}</div>
            @enderror
        </p>   
            <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Valider </button>
        </form>
@endsection

@section("scripts")
    <script src="{{asset('js/select2agent.js')}}"></script>
@endsection
