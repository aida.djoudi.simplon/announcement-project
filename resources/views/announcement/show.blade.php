@extends('base')
@section("title","view")
@section('content')
    <img img src="{{asset('img/'.$announcement->picture)}}"  
        alt="picture of the announcement" class="d-block w-100 " style="height:38rem;" src="#">
    <br>
    <div class="row">
        <div class="col col-sm-8">
            <div class="underline">
                <p> <strong>détaille de l'annonce</strong> </p> 
            </div>
            <br>
            <div class="description">
                <p>Prix:{{$announcement->price}} €</p>
                <p>Surface Habitable :{{$announcement->area}} m² environ</p>
                <p>Nombre de pièces:{{$announcement->roomsNumber}} Pièces
                    / {{$announcement->roomsNumber-1}} Chambre(s)
                </p>
                <p>Référence :{{$announcement->reference}}</p>
            </div>
            <br>
        </div>
        <div class="col col-sm-4">
            <div class="underline ">
                <p> <strong>Votre Agent</strong></p> 
            </div>
            <div class="description">
                <p>{{$announcement->agent->lastName.' '.$announcement->agent->firstName}}</p>   
            </div>
        </div>
    </div>
@endsection


