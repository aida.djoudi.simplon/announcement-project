<?php

namespace App\Http\Controllers;

use App\Models\Agent;
use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File; 
use Illuminate\Support\Facades\Storage;

class AnnouncementController extends Controller
{
    /**
     * Display a listing of the resource for the admin.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all announcements 
        //select * from agents inner join announcements on agents.id = announcements.agent_id
        $announcements = Announcement::latest()->get();
        $agents = Agent::get();
        //Display announcements by agent 
        if(\request('agent'))
        {
        // select * from agents inner join announcements on agents.id = announcements.agent_id where announcements.agent_id =?
            $announcements = Announcement::where('agent_id',request('agent'))->get();
        }
        //Display announcements by price
        if(\request('start_price') )
        {
            $announcements = Announcement::whereBetween('price',[request('start_price'),request('end_price')])->get();
            // $announcements = Announcement::where('agent_id',request('agent'))->get();
        }
        //transmit announcements to the view
        return view("announcement.index", compact("announcements","agents"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $agent_id = null;
        $agents = Agent::orderBy('id')->get();
        return view("announcement.edit",compact("agents","agent_id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //  1.validation of the form */
        $request->validate([
            "reference" => 'required',
            "picture" => 'required|mimes:jpg,png,jpeg|max:5048',
            "price" => 'required',
            "area" => 'required',
            "roomsNumber" => 'required',
            //don't forget to add require for agent_id
            "agent_id" => 'required'
        ]);

        //  2.get new name of picture that contain the reference of announcement
            $newPictureName = time() . '_' . $request->reference . '.' .
            $request->picture->extension();

        // 3.upload the picture in public/img 
            $request->picture->move(public_path('img'),$newPictureName);

        // 4.add announcement in database
        Announcement::create([
            "reference" => $request->reference,
            "picture" => $newPictureName,
            "price" => round($request->price,2),
            "area" => $request->area,
            "roomsNumber" => $request->roomsNumber,
            "agent_id" => $request->agent_id
        ]);
        
        // 4.redirect to index page and get message success
        return redirect(route('announcements.index'))->with('success','Votre annonce a bien été ajoutée');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function show(Announcement $announcement)
    {
        //Get the announcement selected 
        return view("announcement.show", compact("announcement"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function edit(Announcement $announcement)
    {
        $agents = Agent::orderBy('id')->get();
        $agent_id=$announcement->agent_id;
        return view("announcement.edit", compact("announcement","agents","agent_id"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announcement $announcement)
    {
         // 1.validation 
        $rules = [
            "reference" => 'required',
            "price" => 'required',
            "area" => 'required',
            "roomsNumber" => 'required',
            "agent_id" => 'required'
        ];
        // if a new picture is send it
        if ($request->has("picture")) {
        // we add validation for the picture"
        $rules["picture"] = 'required|mimes:jpg,png,jpeg|max:5048';
        }
        $this->validate($request, $rules);
        // 2. we upload the picture in "public/img"
        if ($request->has("picture")) {
        //we delete the old picture
            File::delete("img/$announcement->picture");
        //we create a new name of the new picture
            $newPictureName = time() . '_' . $request->reference . '.' .
            $request->picture->extension();
        // we upload the new picture in "public/img"
            $request->picture->move(public_path('img'),$newPictureName);
        }

        // 2.update the announcement information
        $announcement->update([
            "reference" => $request->reference,
            "picture" => isset($newPictureName) ? $newPictureName : $announcement->picture,
            "price" => $request->price,
            "area" => $request->area,
            "roomsNumber" => $request->roomsNumber,
            "agent_id" => $request->agent_id
        ]);
        // 3.redirect to index page
        return redirect(route("announcements.index"));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcement $announcement)
    {
        //delete the picture
        File::delete("img/$announcement->picture");
        //delete the announcement
        $announcement->delete();
        // Redirection to index page"
        return redirect(route('announcements.index'));
    }
    /**
    * Display a listing of the resource for the public.
    *
    * @return \Illuminate\Http\Response
    */
    public function list()
    {
        //get all announcements
        $announcements = Announcement::latest()->get();
        //transmit agents to the view
        return view("home", compact("announcements"));
    }
}
