<?php

namespace App\Http\Controllers;

use App\Models\Agent;
use Illuminate\Http\Request;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all agents 
        $agents = Agent::latest()->get();
        //transmit agents to the view
        return view("agent.index", compact("agents"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("agent.edit");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //  1.validation of the form
        $this->validate($request, [
            'firstName' => 'bail|required',
            "lastName" => 'bail|required'
        ]);

        // 2.add agent in database
        Agent::create([
            "firstName" => ucfirst($request->firstName),
            "lastName" => strtoupper($request->lastName)
        ]);

        // 3.redirect to index page and get message success
        return redirect(route('agents.index'))->with('success','Agent a bien été ajouté');
        

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function edit(Agent $agent)
    {
        return view("agent.edit", compact("agent"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agent $agent)
    {
        // 1.validation 
        $rules = [
            "firstName" => 'bail|required',
            "lastName" => 'bail|required'
        ];
        $this->validate($request, $rules);

        // 2.update the agent information
        $agent->update([
            "firstName" => $request->firstName,
            "lastName" => $request->lastName
        ]);

        // 3.redirect to index page
        return redirect(route("agents.index"));

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agent $agent)
    {
    //delete the agent
        $agent->delete();
    // Redirection to "agents.index"
        return redirect(route('agents.index'));
    }
}
