<?php

namespace App\Models;

use App\Models\Agent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class announcement extends Model
{
    use HasFactory;
    protected $fillable = ['reference', 'picture','price','area','roomsNumber','agent_id'];

    //Get the Agent that owns the announcement
    public function Agent()
    {
        return $this->belongsTo(Agent::class);
    }
    
}
