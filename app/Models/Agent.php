<?php

namespace App\Models;

use App\Models\Announcement;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Agent extends Model
{
    use HasFactory;
    protected $fillable = [ "firstName", "lastName" ];

    // Get the announcements for the Agent.
    public function announcements()
    {
        return $this->hasMany(Announcement::class);
    }
}
