<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AgentController;
use App\Http\Controllers\AnnouncementController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// display list of announcement 
Route::get('/', [App\Http\Controllers\AnnouncementController::class, 'list'])->name('home');

//add the resource route of Agent
Route::resource("agents", AgentController::class);

//add the ressource route of announcement
Route::resource("announcements", AnnouncementController::class);


